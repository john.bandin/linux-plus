# Lesson 1.4 System Services and Scheduling Services

![](/Images/Linux%20Image%20(7).png)

## Lab Goals/Summary

- In this lab we will cover various commands that show us processes in our system. 
- In this lab we also cover how to kill a process.
- In this lab I am using a Debian-based Linux OS.

1. First we will start a terminal on our Linux OS.

![](/Images/kali%20terminal.png)

2. The first command we're going to cover is the `pgrep` command. this will show what a specific service's process ID is.

`pgrep -l ssh`

![](/Images/pgrep%20command.png)

3. The next command we will cover is the `top` command. This shows us processes that are consuming CPU or memory. 

`top`

![](/Images/top%20command.png)

4. The next command we are going to cover is the `ps` command. We are going to use the command to view processes and the corresponding PID.

`sudo ps -ef`

![](/Images/ps%20-ef%20command.png)

5. Next lets isolate a PID and kill that process. We will start firefox, determine what the PID is using the `netstat` command. Check with the `ps` command, and then kill the process using the `kill -9` command.

```
sudo netstat -antp
sudo ps -A | grep firefox-esr
sudo kill -9 [PID]
sudo ps -A
```
![](/Images/kill%20command.png)

6. Next we will cover the `lsof` command. the `lsof` command opens files and associates the process IDs, it will also identify users with files open.

`sudo lsof`

![](/Images/lsof%20command.png)

7. The next command we will go over is the `pstree` command. This command will show us a processes in a hierarhcal fashion. This command shows us the "**child**" processes that are created from "**parent**" processes.

![](/Images/pstree%20command.png)

8. The last command we will execute is the `uptime` command. This command will show us how long our device and CPU has been on.

# Lab Summary

- In this lab we covered how to view processes on our Linux machine, and also how to kill a process on a Linux machine.