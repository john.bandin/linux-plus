# Lesson 1.4 System Services and Scheduling Services

![](/Images/Linux%20Image%20(6).png)

## Lab Goals/Summary

- In this lab we are going to download the "**ngnix**" service, start the service and make it a daemon using `systemctl` and then remove that service using the `systemctl` command.
- In this lab we are also going to start a cron job that reboots the linux machine every 20 hours.
- In this lab I am using a Debian-based Linux OS. (kali linux)

1. First we will start a terminal on our Linux OS.

![](/Images/kali%20terminal.png)

2. Next we will install the ngnix service using the `apt` package installer.

`sudo apt update`

`sudo apt install nginx`

3. Next to make this web service a daemon we will use the `systemctl` command to start the service, and then we will "**enable**" the service. The `enable` command is what makes this service start at boot.

```
sudo systemctl start nginx
sudo systemctl enable nginx
```

![](/Images/systemctl%20comand%20start.png)

4. Now lets check the status of our service, and then disable the service using the "**disable**" command.

![](/Images/systemctl%20status%20disable.png)

5. Now lets create a cronjob. To start/edit a cronjob execute the `crontab -e` command. And then we have to select an editor. I am going to use Nano.

` crontab -e`

6. Now lets put a cron job in the file. 

`0 */20 * * * /sbin/shutdown -r now`

![](/Images/crontab%20editing.png)

7. This will now run every 20 hours refer to the photo below to see how the timing works in a cronjob.

![](/Images/cronjob%20info.png)

# Lab Summary

- In this lab we covered how to install a service package, start the service and create a daemon for that service. We also learned how to start a cronjob and edit the cron file. 