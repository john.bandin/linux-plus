# Lesson 1.5a Interface Management and Name resolution

## Lab Goals/Summary

- In this lab we will view our NIC and make sure we understand what the interface ID is.
- In this lab we will take a look at the `/etc/hosts` file and discuss how this file does local name resolution and how we could manipulate this file.
- For this lab I am using a Debian-based Linux OS (Kali Linux)

1. First we must open up our terminal. 

![](/Images/kali%20terminal.png)

2. Now lets run the `ip a` command to view the network interfaces on our device. 

`ip a`

![](/Images/ip%20a%202.png)

3. Here we can see we have two interfaces. A loopback interface which is used for locally testing the functionality of our NIC, and the `eth0` interface which our connection out to our network.

4. Next lets take a look at the routing table on our device. We do this by running the `ip route` or the `route` command.

![](/Images/ip%20route%20cmd.png)

![](/Images/route%20cmd.png)

5. As we can see above we only have two routes. IF we wanted to add a new static route, the syntax would be. 

`route add -net 192.168.2.0 netmask 255.255.255.0 dev eth0`

6. Now lets take a look at our `hosts` file. This file will do local name resolution.

`cat /etc/hosts`

![](/Images/etc%20hosts%20file.png)

7. If we wanted to add a local name to be resolved to ip we would edit the `hosts` file using a text editor and by following the syntax below.

```
sudo nano /etc/hosts

192.168.10.169 example.com

```

![](/Images/editing%20hosts%20file.png)

8. Another command that will allow us to view the DNS servers is the `/etc/resolv.conf` file.

`cat /etc/resolv.conf`

![](/Images/etc%20resolv.png)

9. One more command we will cover is the `hostnamectl` command. This command will show the hostname of your device, OR allow you to change the hostname.

`hostnamectl`

![](/Images/hostnamectl%20command.png)

# Lab Summary

- In this lab we learned how to view our network inteface cards and also how manipulate and view our local name resolution.