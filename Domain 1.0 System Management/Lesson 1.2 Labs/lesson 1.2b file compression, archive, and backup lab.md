# Lesson 1.2b File Compression, Archiving and Backup lab

![](/Images/Linux%20Image%20(3).png)

## Lab Goals/Summary

- For this lab we are going to create files and compress or "**zip**" the files into a single zipped folder. We will also unzip the files. 
- We are going to use several different compression methods.
- We are also going to use the "**dd**" command to copy data from a source file to a new destination file.
- For this lab I am using a Debian-based Linux OS (Kali Linux)

1. Open up the terminal on your linux machine

![](/Images/kali%20terminal.png)

2. First lets create 3 files using the `touch` command.

`touch file1 file2 file3`

3. Now let's create an "**archive**" or a "**zipped**" folder.

`tar -cvf archive.tar file1 file2 file3`

![](/Images/touch%20and%20tar.png)

4. Now lets take a look at the files in our "**archive**" file using the `ls` command and the `tar -tf` command.

![](/Images/tar%20command%20-tf.png)

5. If we want to add another file to our archive we can do that using the `tar -rf` command. 

```
touch file4
tar -rf archive.tar file4
tar -tf archive.tar
```

![](/Images/tar%20-rf%20command.png)

6. Now we can delete the individual files, and then extract from our archive file whenever we need them.

```
rm -r f*
ls -la
tar -tf archive.tar
tar -xf archive.tar
ls -la
```

7. Now lets move on to using the `dd` command. The `dd` command allows to copy from a source file; or the "**input file**" and duplicate/copy the exact data onto a destionation file; or the "**output file**".

```
nano newddfile.txt
# paste in the short story from the GITLAB repo
dd if=/path/newddfile.txt of=/path/newoutputfile.txt 
cat newoutputfile.text
```

![](/Images/dd%20command.png)

