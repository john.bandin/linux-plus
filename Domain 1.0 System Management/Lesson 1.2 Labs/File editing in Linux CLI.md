# Lesson 1.2a File Editing in Linux CLI

![](/Images/Linux%20Image%20(2).png)

## Lab Goals/Summary

- In this lab we are going to cover CLI commands that identify file paths and full directory paths.
- We are going to use two different text editors on the CLI; VI and Nano
- VI and VIM are what traditional and advanced Linux users will typically prefer. VI is more robust and offers more features than the begineer friendly nano text editor.
- nano is a text editor that is not as feature rich as VI but has an easier learning curve than VI/VIM.
- For this lab I am using a Debian-based Linux OS (Kali Linux)

1. Open up the terminal on your linux machine

![](/Images/kali%20terminal.png)

2. First lets use and view the `file` command on our systems. Lets try to find any files under our users directory that are over 1KB in size. `find /home/[USERNAME]/ -name "*/.txt -size +1k`

```
nano /home/[username]/fileover1k.txt # paste in the short story located at the root of our GIT Repo

Ctrl + V
Ctrl + O
Ctrl + X

find /home/[USERNAME] -name "*.txt" -size +1k

```

![](/Images/find%20command.png)

3. View the output and notice all the files over 1K. We can use this "find" command to search for any directory or file within our system. Now let's use the `which` command to see where applications are stored on our system. Let's see where the `cat`/ command and feature is stored on our system, and lets also check where the coding language `python` is stored. `which cat` | `which python`

![](/Images/which%20command.png)

4. Now let's move on to our text editor. We will first go over Nano. To use Nano to create text files on our cli the syntax is simple. `sudo nano [file-name]`. At the bottom of the Nano text editor all the shortkey options are shown.

```

Ctrl + X == Exit 
Ctrl + O == Save changes
Ctrl + U == Paste clipboard contents
Ctrl = K == Cut highlighted text

```

5. There are plenty of other options in Nano. One key difference between nano and VI/VIM is the learning curve for VI/VIM and the robust features VI offers. To create a file using VI/VIM the command is `vi newfileforvi.txt`

6. To insert text into VI you must hit the "**i**" key on your keyboard. To escape "**insert**" mode hit the "**esc**" key on your keyboard. To exit VI type "**:wq**" to save and quit VI. ***NOTE THIS IS JUST A SIMPLE DEMONSTRATION OF VI/VIM. THERE IS A LOT MORE FUNCTIONAILITY AND FEATURES VI/VIM CAN DO***

![](/Images/vi%20linux.png)

# Lab Summary

- In this lab our goal was to get familiar with the CLI text editors. VI/VIM is the more feature rich and robust CLI editor. This text editor has a LOT of shortcuts that are sometimes hard to remember. [VI Cheatsheet](http://www.linux-admins.net/2011/01/vi-cheat-sheet.html)


```
find - # The find command in Linux is a powerful utility used to search for files and directories based on various criteria such as names, types, sizes, and more. It's a versatile tool for locating files in a directory tree. Here's how to use the find command:

which - # The which command in Linux is used to locate the executable file associated with a given command or program. It helps you determine the exact path of the executable that will be executed when you run a command in the terminal. Here's how to use the which command:
```

