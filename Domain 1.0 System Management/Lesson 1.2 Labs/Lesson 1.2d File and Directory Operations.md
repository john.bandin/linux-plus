# Lesson 1.2d File and Directory Operations
![](/Images/Linux%20Image%20(5).png)

## Lab Goals/Summary

- In this lab we will cover a range of commands that will allow us navigate our Linux Machine, view files and directories, edit the output on our cli, and filter our searches.
- In this lab I will be using a Debian-based OS. (Kali Linux)

1. First we will need to open a terminal on our machine.

2. Now lets use the `cd` command to change the working directory we are in, on our CLI. 

```
cd .
cd ..
cd /
cd /[directory]/[path]
cd ~
```

![](/Images/cd%20command.png)

3. Now on your machine change your working directory to the users "**Desktop**" and then using two dots `..` go back to the root of the system. Each time you run the `cd ..` command also run the `pwd` command. This command will show the present working directory you are in.

`pwd`

![](/Images/pwd%20comand.png)

4. Now lets head back to the users "**desktop**" and run the `tree` command. You may need to install the tree command using the `apt` package installer.

```
cd /home/[user]/Desktop
tree
```

![](/Images/tree%20command.png)

5. The `tree` command allows you to see all the directories/sub-directories and files within those directories. Now lets use the `ls` command and the different options to view information about the directories and files in our "**Desktop**".

```
ls 
ls -l
ls -a
ls -la
```

![](/Images/ls%20-l.png)

![](/Images/ls%20-la%20dev.png)

6. Now lets create more directories and sub directories under the "**Desktop**" folder.

```
mkdir project
mkdir -p projects/labs
mkdir -p projects/solutions
```

![](/Images/mkdir%20command.png)

7. The `mkdir` command will create directories and sub-directories. Now let's create a file using the `touch` command and move the file into another directory using the `mv` command.

```
touch /home/johnny/Desktop/filedirectory.txt
mv /home/johnny/Desktop/filedirectory.txt /home/johnny/Desktop/project/labs
tree /home/johnny/Desktop/project
```

![](/Images/touch%20and%20mv%20command.png)

8. Now lets add some text to the new file we created. Using `nano` we will add the "**short story**" file text to the new file we created. And then we will use five different commands to view the output of the file.

```
nano /home/johnny/Desktop/project/labs/filedirectory.txt
"Ctrl + V"
cd /home/johnny/Desktop/project/labs
cat filedirectory.txt
more filedirectory.txt
less filedirectory.txt
head filedirectory.txt
tail filedirectory.txt
```
![](/Images/cat%20command.png)

9. Now lets filter out some of the output from the text using the `grep` command.

![](/Images/grep%20command.png)

10. Now lets full remove the "**projects**" folder using the `rm` command. First we will navigate to the Desktop folder and run the following commands

```
sudo rm -rf projects
tree 

```

![](/Images/rm%20command.png)

# Lab summary
