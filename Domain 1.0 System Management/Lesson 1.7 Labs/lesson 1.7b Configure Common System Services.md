# Lesson 1.7b Configure Common System Services

![]()

## Lab Goals/Summary

- In this lab we will cover configuring our time correctly on our system so that common services we may run on our Linux server have synchronization.
- For this lab I am using a Debian-based Linux OS. (Kali Linux)

1. First we will open our linux terminal. 

![](/Images/kali%20terminal.png)

2. Next we will run the `date` command to see the current date on our system. 

`date`

![](/Images/date.png)

3. Next if we wanted to configure the time and date we would run the `timedatectl`

![](/Images/timedatectl.png)

4. If we wanted to change/view our local language and keyboard setting we would run the `localectl` command. 

![](/Images/localectl.png)

# Lab summary

- In this lab we covered some basic commands that we can use to configure some common setting on our Linux machine from the CLI.
