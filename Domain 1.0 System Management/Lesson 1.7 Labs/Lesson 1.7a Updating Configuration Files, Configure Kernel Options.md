# Lesson 1.7a Updating Configuration Files, Configure Kernel Options

![]()

## Lab Goals/Summary


- In this lab we will use the `apt` package installer to view information about our installed software.
- In this lab we will use CLI commands to view information about Linux Kernel and information about the OS we are using
- For this lab I am using a Debian-based OS (kali linux)

1. First we will open our Linux terminal

![](/Images/kali%20terminal.png)

2. Next we will view information about our NMAP software using the `apt show` command.

`sudo apt show nmap` 

![](/Images/apt%20show%20command.png)

3. This command will show us all the information that was downloaded when we installed the NMAP software. Next we will check where our `apt` package repository sits with the `cat /etc/apt/sources.list` command.

![](/Images/apt%20sources%20list.png)

4. Now let's move onto finding information about our kernel. To display the kernel version and name run the following commands.

`uname -r`

`uname -a`

![](/Images/uname%20command.png)

5. Now,lets do some more discovery of our kernel modules. First we will run the `lsmod` command to see the loaded kernel modules on our system.

![](/Images/lsmod.png)

6. Now, lets dig deeper and find out more information about specific kernel modules using the `modinfo` command and specifying a kernel module name

`modinfo drbg`

![](/Images/modinfo%20command.png)

7. IF we wanted to remove kernel modules we could run the `rmmod` command and specify the kernel module we want removed. 

8. Now lets take a look at our kernel parameters using the `sysctl -a` command.

![](/Images/sysctl%20-a.png)

9. the last command we will cover is the `dmesg` command. This command will display a ton of information about the kernel during the boot process. We will use the `grep` command to just show us output for our USB busses. 

![](/Images/dmesg%20command.png)






