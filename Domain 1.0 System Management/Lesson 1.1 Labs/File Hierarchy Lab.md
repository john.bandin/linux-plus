# Lesson 1.1a Filesystem Hierarchy System Lab 1

![FHS](/Images/fhs.png)

## Lab Goals/Summary

- You have been tasked with installing a new application on your Linux System. When you install this application where will its configuration files be installed/stored? 
- We also want to see the metadata for our configuration files
- Discuss why the Linux FHS makes automating installations easier across all Linux distributions
- For this lab we will be using a debian-based Linux OS (kali Linux)



## Lab Walkthrough

1. Logon too your Debian-based Linux OS. ![kali-login](/Images/kali%20linux%20login.png)
2. Open up a terminal ![kali-terminal](/Images/kali%20terminal.png)

3. Install the "**tree**", "**nano**", and "**SSH**" applications via the command line. 

4. `sudo apt install -y tree nano ssh` 

![install tree nano ssh](/Images/kali%20install%20%20tree%20nano%20and%20ssh.png)

5. Now let's navigate to the system wide configuration files of nano and ssh. 
```
cd /  
cd /etc/
ls -la nanorc
more nanorc
-----------
cd /etc/ssh/
ls -la ssh
more ssh_config
```

6. By typing the following commands above we see the configuration files for both of the applications we installed. ![nanorc navigation](/Images/navigating%20to%20nanorc.png) ![ssh_config navigation](/Images/navigating%20to%20ssh_config.png)


7. Now that we found the configuration files. Lets take a look at the metadata. 
```
cd /etc/
stat nanorc
------------
cd /etc/ssh
stat ssh_config
```

8. Lets analyse the stat command output.

![stat command](/Images/stat%20copmand%20ssh_config.png)

9. With the stat command we can see the metadata associated with the ssh_config file. Notice and take note of the modify time. We are going to open the ssh_config with our nano editor we installed make a simple change, and then run our stat command again. 

10. `sudo nano /etc/ssh/ssh_config` 

![nano ssh_config](/Images/nano%20ssh_config.png)

11. Using the arrow keys go to the line with the `# site-wide defaults...` and delete the **hashtag**. Hit the "ctrl + o" keys on your keyboard to save the change. The hit the "ctrl + x" keys on your keyboard  to exit the nano editor, and run the **stat** command again. 

![](/Images/changing%20ssh_config.png)

12. `stat /etc/ssh/ssh_config` 

![](/Images/second%20stat%20command.png)

13. Notice the modify time metadata column changed. The **stat** command will show us additional information about our files in Linux.

14. Now with the class let's discuss why the Linux FHS helps automate across any Linux Distribution when you are installing software and making changes to configuration files.

