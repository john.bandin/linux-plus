# Lesson 1.1c Device Types in Dev

![boot process](/Images/cat%20proc_info.png)

## Lab Goals/Summary

- list all the device types in the "**/dev**" directory and discuss the output. 
- Use a CLI command to display the information on the motherboard busses.
- Display files contents about the CPU on the device.
- Use CLI commands from the terminal to gain knowledge about the hardware on the device.
- For this lab we will be using a debian-based OS (kali linux) running in VMWare Fusion on a ARM Based macOS. ***some of the devices in /dev may differ based on the distribution and how the OS is deployed, as a VM, Cloud-based VM, or bare metal deployment***

1. Access the terminal on your debian-based Linux OS 

![](/Images/kali%20terminal.png)

2. Now navigate to the "**/dev**" folder, and execute the "**ls**" command. 

![](/Images/ls%20dev.png)

3. Take note of all the files and devices in the /dev directory. Which command can we run to see the permissions, owners of all the files? 

![](/Images/ls%20-la%20dev.png)

4. When we run the `ls -la` command we can see if there is directories or files and the permissions on the files.

![](/Images/ls%20-la%20dev.png)

5. Now lets check and detect the storage devices in our system and in the /dev folder. Run the `lsblk` command on the terminal.

![](/Images/lsblk%20command.png)

6. In this deployment of Kali Linux on a VM the main storage device is `/dev/nvme0n1p2`. Typically the main storage drive would be /dev/sda1. Another way to view the storage devices is to use the `fdisk -l` command. 

![](/Images/fdisk%20-l%20command.png)

7. The `fdisk` command will let you manipulate disks, but we use it in our example to list the information about detected storage devices. Another command we have to list detected devices is the `parted -l` command.

![](/Images/parted%20-l.png)

8. Another command we can use to display information on the disks and devices is the `lshw` command. ***if the `lshw` command is not available you may have to install it by running the apt package installer. `apt install lshw`***

![](/Images/lshw%20command.png)

9. Another command we can use is the `udevadm` to interact with the device manager in the linux kernel. To use the `udevadm` we will have to pass additional arguments. For my Kali Linux VM our disk in named `nvme0n1p1`. The full command is `udevadm info --query=all --name/dev/nvme0n1p1`

![](/Images/udevadm%20info.png)

10. Now lets take a look at our hardware busses on our linux machine. Execute the `hwinfo` command to view detailed information about the hardware. This command will give a lot of output that is hard to parse through. We can use the `grep` command to search for specific information. `hwinfo | grep Ethernet`

![](/Images/hwinfo%20grep%20command.png)

11. The `hwinfo` has several other switches we can use to look for specific hardware information. 

```
hwinfo --short --cpu
hwinfo --short --storage
hwinfo --short --netcard
hwinfo --short --usb
```

![](/Images/hwinfo%20--short%20command.png)

11. There are other commands that will give less output, but will show us detailed information on specific hardware busses. The `lspci` and `lsusb` command are two examples. 

12. We want to be able to parse the information and make it human readable, so we will add the -mm switch to the command. `lspci -mm`

![](/Images/lspci%20-mm%20command.png)

13. The `lsusb` command will show us all the USB ports on our hardware.

![](/Images/lsusb%20command.png)

14. Now lets take a look at our CPU utilization. This information is in a file labeled `cpuinfo` under the `proc` directory. This file is created at boot to display current information. `head /proc/cpuinfo`, `less /proc/cpuinfo`, `cat /proc/cpuinfo`. Any of the test display options will work to look at current CPU information.

![](/Images/proc%20info.png)

15. Another file created at boot is the `meminfo` file also stored in the `/proc` directory. To view the `meminfo` file use the same text display options we used above. 

![](/Images/proc%20meminfo.png)

16. For the final step in this lab lets put our cpu info into a file called **cpu information [DD:HH:MM]**. We do this with the by running the `cat` command and outputing the information to a file we create. We will do this in the Desktop Directory under our user. 

```
cd /home/[SUERNAME]/Desktop
touch cpu_information_25:18:10
cat /proc/cpuinfo >> cpu_information_25:18:00
cat cpu_information_25:18:00
```
![](/Images/Screenshot%202023-08-25%20at%206.11.43%20PM.png)


# Lab Summary

## In this lab we used a lot of CLI commands to display the different disks and storage devices on our machine, we also used several commands to display specific hardware information. Takes note of all the output display and get used to navigating through the CLI.



## Commands used

```
lspci - Displays PCI bus information
lsusb - detailed information for devices on the USB bus
hwinfo - detailed hardware information
lsblk - lists information about block devices
fdisk - lists information about detected storage devices
parted - used for managing disk partitions, can also list devices
lshw - provides detailed information about hardware
udevadm - Command interacts witht the device manager in the linux kernel
```

