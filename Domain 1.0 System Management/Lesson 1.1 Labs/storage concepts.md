# Lesson 1.1d storage concepts

![boot process](/Images/storage%20concepts.png)

## Lab Goals/Summary

- View and understand the Logical Volume Manager on the CLI
- View from the CLI if the Linux device has an MBR(master boot record) or GPT (GUID Partition Table) partition.
- Create a folder and share it with the LAN
- For this lab I am using a Debian-based Linux OS (Kali Linux)

1. Login to a terminal on your Debian-based Linux.

![](/Images/kali%20terminal.png)

2. On the Linux CLI there are multiple commands we can use to display the physical volumes, volume groups, and logical volumes.

```
pvdisplay - This command displays detailed information about physical volumes.

vgdisplay - Use this command to view information about volume groups.

lvdisplay - This command provides information about logical volumes.

lvs - The lvs command is another way to list logical volumes, providing a concise output.

pvs - The pvs command lists physical volumes with concise output.

vgs - Use the vgs command to list volume groups with concise output.

lvscan - This command quickly scans for logical volumes.

pvscan - The pvscan command scans for physical volumes.

vgscan - Use the vgscan command to scan for volume groups.

lvm - LVM also provides an interactive utility named lvm that offers a more comprehensive way to manage LVM components.
```

3. To view if our Linux device has an MBR or GPT partition their a couple commands we can run. 

```bash
sudo fdisk -l /dev/[YOUR_DRIVE]
```

![](/Images/fdisk%20-l%20command%20dev.png)

4. If you look at the output above and view the "**disklabel type**" our linux device is using GPT. Another command we can use to view the partition type is 

```bash
sudo parted /dev/[YOUR_DRIVE] print
```

![](/Images/parted%20print%20command.png)

5. If you view the output above and look at the "**partition table**" we can see that we are using the "**GPT**" partition. Another command we can use specifically for GPT partition is the `gdisk` command. 

![](/Images/gdisk%20command.png)

6. Now lets create a folder on Desktop and share the folder with our Network. We will create the "**project**" directory with two sub-directories, and create a file under the "solutions" folder named "**solution1**". ***Copy and paste the short story from our GITLAB repository.

```bash
mkdir -p /home/[USERNAME]/Desktop/Project/labs
mkdir -p /home/[USERNAME]/Desktop/Project/solutions
nano /home/[USERNAME]/Desktop/Project/solutions/solutions1.txt
```

![](/Images/Screenshot%202023-08-26%20at%208.01.05%20AM.png)

7. Now we will have to install "**samba**" 

```bash
sudo apt install samba
```

![](/Images/apt%20install%20samba.png)

8. Now we have to edit the samba configuration file located in the `/etc` folder.

```bash
sudo nano /etc/samba/smb.conf

[projects]

comment = Shared Folder
path /path/to/your/folder
read only = no
browsable = yes

```

![](/Images/samba%20config.png)

9. Now we have to make sure that our shared folder has the correct permissions. 

```bash
sudo chown 744 [folder_name]
```

![](/Images/chown%20744%20permissions.png)

10. Now we must restart the **samba** service to apply the changes. 

```bash
sudo service smbd restart
```

![](/Images/service%20smbd%20restart.png)

11. Samba uses its own user accounts, separate from the system users. You'll need to add Samba users and set passwords for them: `sudo smbpasswd -a username`

# Lab Summary

## 

In this lab we covered volumes and disks in a linux system, and learned how to view what type of partition we are using on our machine. We also created a shared folder using samba. Samba is a file sharing service that can be used in a Unix and even windows environment. The samba share we created uses file-sharing. 

## Commands Used

```

apt install samba - this command installs the samba service
mkdir -p - this command creates a directory and sub-directory
smbpasswd - This command sets a password for a user accessing our samba share
chown - this command sets the permissions on a user, group or object
fdisk - The fdisk command is a utility to manipulate disk partition tables.
parted - The parted command is another utility that provides information about disk partitions and partitioning schemes.
gdisk - The gdisk command is a specialized tool for GPT disks. If you have GPT partitions, you can use it to inspect GPT-specific information.

```
