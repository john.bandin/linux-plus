# Lesson 1.1b Linux Boot Process Lab 1

![boot process](/Images/Linux%20Image%20(1).png)

## Lab Goals/Summary

- Understand how to deploy a Linux OS to a Type 2 hypervisor (VMWare Player) using an .iso or .ova file.
- Take note of the different boot options between a RHEL-based and a Debian-based Linux Distribution.

## Lab walk-through

1. Deploy a Linux machine on VMWare player. Follow instructor for this lab.