# Lesson 2.1b Authentication 

## Lab Goals/Summary

- In this lab we will take a look at the `/etc/passwd` and the `/etc/shadow` file. 
- For this lab I am using a Debian-based Linux OS (Kali Linux)

1. First we will open our terminal.

![](/Images/kali%20terminal.png)

2. Now from the terminal let's take a look at the `/etc/passwd` file using the `more` command.

`more /etc/passwd`

![](/Images/more%20etc%20passwd.png)

3. Now let's actually search in this "**passwd**" file for a specific user. The "**passwd**" file contains all the usernames for the Linux system. 

`cat /etc/passwd | grep root` 

![](/Images/cat%20etc%20grep.png)

4. From the image we can see that we just see the `root` user now. Now let's take a look at the `shadow` file. The shadow file stores all the passwords for our users. The passwords will be hashed and not stored as plaintext in our system.

`cat /etc/shadow`

![](/Images/cat%20etc%20shadow.png)

# Lab summary